#More information at http://mikeos.sourceforge.net/write-your-own-os.html
#dd status=noxfer conv=notrunc 


main.flp: main.bin
	@dd if=$< of=$@

main.bin:
	@nasm -f bin -o $@ *.asm

clean:
	rm main.bin
	rm main.flp
run:
	qemu-system-i386 -fda main.flp
